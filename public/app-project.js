var app = new Vue({
    el: '#app',
    data: function () {
        return {
		}
    },
	computed: {
    	// Animate spike
		animateSpike: function () {
			// mouse move
			mouseMoveMethod = function (event) {
				var pageYOffset = window.pageYOffset;
				var docWidth = $(window).outerWidth();

				// Если скролл больше 1400
				if (pageYOffset >= 1400) {
					$('.cover__spike').css(
						'transform', 'translateX(0px), translateY(0px);'
					);
					$('.seeds').addClass('seeds_disable-mouse-motion');
				}
				// Если ширина документа меньше
				if (docWidth < 1200) {
					$('.cover__spike').css(
						'transform', 'translateX(0px), translateY(0px)'
					);
					$('.seeds').addClass('seeds_disable-mouse-motion');
				}
				// В остальных случаях
				if (pageYOffset < 1400 && docWidth > 1200) {
					$('.cover__spike').css(
						'transform', 'translateX(' + (event.pageX / 200) + 'px)'
						+ 'translateY(' + ((event.pageY / 200)) + 'px)'
					);
					$('.seeds')
						.removeClass('seeds_disable-mouse-motion')
						.css(
						'transform', 'translateX(' + (event.pageX / 200) + 'px)'
						+ 'translateY(' + ((event.pageY / 200)) + 'px)'
					);
				}
			};
			if ($(window).width() >= 1200) {
				// document.addEventListener('mousemove', mouseMoveMethod);
			}
		},
		// Cut and paste content
		cutAndPasteСontent: function () {
			function move() {
				$('.cover__special-wrapper').appendTo('#app');
				$('.header').prependTo('#app');
				$('.seeds').prependTo('.composition');
			}

			window.addEventListener('load', function (event) {
				if ($(window).width() < 1200) {
					move();
				}
			});
		},
		// Scroll to element
		scrollToEl: function () {
			window.addEventListener('load', function (event) {
				if ($(window).width() < 1200) {
					function fadeIn(element) {
						$(element).each(function (i) {
							var bottomOfObject = $(element).offset().top + $(element).outerHeight();
							var bottomOfWindow = $(window).scrollTop() + $(window).outerHeight();
							var elementClass = element + '_active';
							elementClass = elementClass.replace(/\./g, "");
							if (bottomOfWindow > bottomOfObject) {
								$(element).addClass(elementClass);
							}
						});
					}

					document.addEventListener('scroll', function (event) {
						fadeIn('.composition__title');
						fadeIn('.composition__list');
						fadeIn('.composition__vitamins');
						fadeIn('.bottle__title');
						fadeIn('.bottle__text');
						fadeIn('.bottle__list');
						fadeIn('.product-first__text-wrapper');
						fadeIn('.product-second__text-wrapper');
						fadeIn('.product-third__text-wrapper');
						fadeIn('.product-fourth__text-wrapper');
						fadeIn('.seeds__seed_1');
						fadeIn('.seeds__seed_2');
						fadeIn('.seeds__seed_3');
						fadeIn('.seeds__seed_4');
						fadeIn('.seeds__seed_5');
						fadeIn('.seeds__seed_6');
						fadeIn('.seeds__seed_7');
						fadeIn('.seeds__seed_8');
					});
					setTimeout(fadeIn('.cover__text-wrapper'), 3400);
				}
			});
		},
		// Show first slide
		showFirstSlide: function (event) {
			// show ANIMATE : begin
			// * cover
			function showCover() {
				$('.cover__background').fadeIn(500);
			}
			// * header
			function showHeader() {
				$('.header').animate({
					top: '0',
					opacity: '1'
				}, 500);
			}
			// * spike
			function showSpike() {
				$('.cover__spike').fadeIn(300);
			}
			// * show text
			function showText(element) {
				// Расстояние от скролла до верха страницы
				var valueToTopDoc = $(document).scrollTop();
				var el = $('.cover__text.cover__text_first');
				// Событие загрузки страницы (показываем первый текстовый блок)
				$(el).addClass('cover__text_first_active');
				// Событие скролла документа
				document.addEventListener('scroll', function (event) {
					var valueTop = $(window).scrollTop();
					// Если проскроллли меньше 800 от верха документа
					if (valueTop < 200) {
						$(el).addClass('cover__text_first_active');
					}
					// Если проскроллли больше 800
					else {
						$(el).removeClass('cover__text_first_active');
					}
				});
			}
			// * call animate function
			window.addEventListener('load', function (event) {
				setTimeout(showCover, 2300);
				setTimeout(showHeader, 2800);
				setTimeout(showSpike, 3100);
				if ($(window).width() >= 1200) {
					setTimeout(showText, 3400);
				}
			});
			// show ANIMATE : end
		}
	}
});

// skrollr init : начало
$(function () {
	if ($(window).width() >= 1200) {
		skrollr.init();
	}
	else {
		skrollr.init().destroy();
	}
});
// skrollr init : конец

// прелоадер : начало
(function () {
	$('.preloader__number-value').animate({ num: 100 - 3 }, {
		duration: 1800,
		step: function (num) {
			this.innerHTML = (num + 3).toFixed(0)
		}
	});

	function fadeOutPreloader() {
		$('body').addClass('body_active');
		$('.preloader').fadeOut(550);
	}

	TweenMax.to('.mask-rect', 1.8, {
		attr:{height: 0},
		yoyo: true,
		repeat: 0,
		ease: Power2.easeInOut
	});

	window.addEventListener('load', function (event) {
		setTimeout(fadeOutPreloader, 1800)
	});
})();
// прелоадер : конец